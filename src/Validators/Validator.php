<?php


namespace App\Validators;


use App\Interfaces\IValidator;

class Validator implements IValidator
{
	private $rules;

	protected $defaultResultStructure = ['is_valid' => false, 'errors' => []];

    public function __construct($rules)
    {
    	$this->rules = $rules;
    }

    /**
     * Method for validate data by the rules in the config
     * @param array $data
     * @return array
     */
    public function validate(array $data): array
    {
    	if (!is_iterable($data)) {
			throw new \InvalidArgumentException("Data for validation should be an iterable");
		}
		$validationResult = [];
		foreach ($this->rules as $fieldName => $validators) {
			$validationResult[$fieldName] = $this->validateField($data[$fieldName] ?? null, $validators);
    	}

		return $validationResult;
    }

	/**
	 * Validate only one field
	 * @param $value
	 * @param array $validators
	 * @return array
	 */
	protected function validateField($value, array $validators): array
	{
		$result = $this->defaultResultStructure;

		foreach ($validators as $validator) {
			$isValid = $validator->isValid($value);
			$result['is_valid'] = count($result['errors']) === 0 ? $isValid : false;
			if ($validator->hasError()) {
				$result['errors'][] = $validator->getErrorMessage();
			}
		}

		return $result;
    }
}
