<?php


namespace App\Interfaces;


interface IValidator
{
    /**
     * Method for validate data by the rules in the config
     * @param array $data
     * @return array
     */
    public function validate(array $data): array;
}