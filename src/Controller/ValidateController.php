<?php


namespace App\Controller;


use App\Validators\Validator;
use Symfony\Component\HttpFoundation\{
	Request, JsonResponse
};
use Symfony\Component\Routing\Annotation\Route;

class ValidateController
{
	/**
	 * @Route("/validate", name="validator", methods={"POST"})
	 */
	public function validate(Request $request, Validator $validator): JsonResponse
	{
		return new JsonResponse($validator->validate($request->request->all()));
	}

	/**
	 * @Route("/", name="index", methods={"POST", "GET", "OPTIONS" })
	 */
	public function index(Request $request): JsonResponse
	{
		return new JsonResponse(['message' => 'Hello']);
	}
}
