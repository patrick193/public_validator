<?php


namespace App\Validators;


use App\Interfaces\{
	IValidatorTypes, IError
};
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class MaxLengthValidatorType implements IValidatorTypes, IError
{
	const MAX_LENGTH = 255;

	/**
	 * @var bool
	 */
	private $hasError = false;

	/**
	 * @var ParameterBagInterface
	 */
	private $params;

	public function __construct(ParameterBagInterface $params)
	{
		$this->params = $params;
	}

	/**
	 * @inheritdoc
	 */
	public function isValid($data): bool
	{
		$maxLength      = $this->getMaxLength();
		$isValid        = (string)strlen($data) <= $maxLength;
		$this->hasError = !$isValid;

		return $isValid;
	}

	/**
	 * Method return an error message if there is exists
	 * @return string
	 */
	public function getErrorMessage(): ?string
	{
		return $this->hasError() ? 'The max length of field should be less or equal ' . $this->getMaxLength() : null;
	}

	/**
	 * Checking if error has been occurred
	 * @return bool
	 */
	public function hasError(): bool
	{
		return $this->hasError;
	}


	protected function getMaxLength(): int
	{
		return $this->params->has('validator.max_length') ? $this->params->get('validator.max_length') : self::MAX_LENGTH;
	}
}
