<?php

namespace App\Interfaces;


interface IError
{
	/**
	 * Method return an error message if there is exists
	 * @return string
	 */
	public function getErrorMessage(): ?string;

	/**
	 * Checking if error has been occurred
	 * @return bool
	 */
	public function hasError(): bool;

}
