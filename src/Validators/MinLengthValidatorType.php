<?php


namespace App\Validators;


use App\Interfaces\{
	IValidatorTypes, IError
};
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class MinLengthValidatorType implements IValidatorTypes, IError
{
	const MIN_LENGTH = 255;

	/**
	 * @var bool
	 */
	private $hasError = false;

	/**
	 * @var ParameterBagInterface
	 */
	private $params;

	public function __construct(ParameterBagInterface $params)
	{
		$this->params = $params;
	}

	/**
	 * @inheritdoc
	 */
	public function isValid($data): bool
	{
		$minLength      = $this->getMinLength();
		$isValid        = (string)strlen($data) >= $minLength;
		$this->hasError = !$isValid;

		return $isValid;
	}

	/**
	 * Method return an error message if there is exists
	 * @return string
	 */
	public function getErrorMessage(): ?string
	{
		return $this->hasError() ? 'The min length of field should be more or equal ' . $this->getMinLength() : null;
	}

	/**
	 * Checking if error has been occurred
	 * @return bool
	 */
	public function hasError(): bool
	{
		return $this->hasError;
	}

	private function getMinLength(): int
	{
		return $this->params->has('validator.min_length') ? $this->params->get('validator.min_length') : self::MIN_LENGTH;
	}
}
