<?php


namespace App\Validators;


use App\Interfaces\{
	IValidatorTypes, IError
};

class RequiredValidatorType implements IValidatorTypes, IError
{
	/**
	 * @var bool
	 */
	private $hasError = false;

	public function isValid($data): bool
	{
		$isValid        = !empty($data);
		$this->hasError = !$isValid;

		return $isValid;
	}

	/**
	 * Method return an error message if there is exists
	 * @return string
	 */
	public function getErrorMessage(): ?string
	{
		return $this->hasError() ? 'This field is required' : null;
	}

	/**
	 * Checking if error has been occurred
	 * @return bool
	 */
	public function hasError(): bool
	{
		return $this->hasError;
	}
}
