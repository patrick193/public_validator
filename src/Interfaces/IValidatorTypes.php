<?php


namespace App\Interfaces;


interface IValidatorTypes
{

	/**
	 * Checking if the data is valid
	 * @param $data
	 * @return bool
	 */
    public function isValid($data): bool;
}